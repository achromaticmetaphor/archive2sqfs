/*
Copyright (C) 2016, 2017, 2018  Charles Cagle

This file is part of archive2sqfs.

archive2sqfs is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

archive2sqfs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with archive2sqfs.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LSL_COMPRESSOR_H
#define LSL_COMPRESSOR_H

#include <cstddef>
#include <future>
#include <memory>
#include <string>
#include <string_view>
#include <utility>
#include <vector>

using namespace std::literals;

#include "sqsh_defs.h"

using block_type = std::vector<char>;

struct compression_result
{
  block_type block;
  bool compressed;

  uint32_t compressed_size() const
  {
    return uint32_t(block.size()) |
           (compressed ? 0 : SQFS_BLOCK_COMPRESSED_BIT);
  }

  uint32_t compressed_metadata_size() const
  {
    return uint32_t(block.size()) |
           (compressed ? 0 : SQFS_META_BLOCK_COMPRESSED_BIT);
  }
};

struct compressor
{
  uint16_t const type;
  virtual compression_result compress(block_type &&) = 0;
  virtual block_type decompress(block_type &&, std::size_t) = 0;
  virtual ~compressor() = default;
  compressor(compressor &) = default;
  compressor(compressor &&) = default;
  compressor & operator=(compressor &) = delete;
  compressor & operator=(compressor &&) = delete;

  std::future<compression_result>
  compress_async(block_type && in,
                 std::launch const policy = std::launch::async)
  {
    return std::async(policy, &compressor::compress, this, std::move(in));
  }

  compressor(uint16_t type) : type(type) {}
};

struct compressor_zlib : public compressor
{
  compression_result compress(block_type &&) override;
  block_type decompress(block_type &&, std::size_t) override;
  compressor_zlib() : compressor(SQFS_COMPRESSION_TYPE_ZLIB) {}
};

#if LSL_ENABLE_COMP_zstd
struct compressor_zstd : public compressor
{
  compression_result compress(block_type &&) override;
  block_type decompress(block_type &&, std::size_t) override;
  compressor_zstd() : compressor(SQFS_COMPRESSION_TYPE_ZSTD) {}
};
#endif

struct compressor_none : public compressor
{
  compression_result compress(block_type && in) override
  {
    return {std::move(in), false};
  }

  block_type decompress(block_type && in, std::size_t) override
  {
    return std::move(in);
  }

  compressor_none() : compressor(SQFS_COMPRESSION_TYPE_ZLIB) {}
};

static inline compressor * get_compressor_for(std::string_view const & type)
{
  if (type == "zlib"sv)
    return new compressor_zlib{};
#if LSL_ENABLE_COMP_zstd
  if (type == "zstd"sv)
    return new compressor_zstd{};
#endif
  if (type == "none"sv)
    return new compressor_none{};
  throw std::runtime_error("unknown compression type: "s.append(type));
}

static std::string_view constexpr COMPRESSOR_DEFAULT = "zlib"sv;

template <typename Compressor>
static bool compress_data(Compressor comp, block_type & out, block_type && in)
{
  if (in.empty())
    return false;

  comp(out, in);

  bool const compressed = out.size() < in.size();
  if (!compressed)
    out = std::move(in);

  return compressed;
}
#endif
