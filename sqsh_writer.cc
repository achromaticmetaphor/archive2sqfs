/*
Copyright (C) 2016, 2017, 2018  Charles Cagle

This file is part of archive2sqfs.

archive2sqfs is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

archive2sqfs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with archive2sqfs.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <cstdint>
#include <filesystem>
#include <memory>
#include <optional>
#include <ostream>
#include <utility>
#include <vector>

#include "compressor.h"
#include "endian_buffer.h"
#include "metadata_writer.h"
#include "sqsh_writer.h"

void sqsh_writer::flush_fragment()
{
  if (!current_fragment.empty())
    enqueue_fragment();
}

template <typename ByteContainer, typename ByteContainer2>
bool compare_range(ByteContainer const & a, ByteContainer2 const & b,
                   std::size_t const off)
{
  return a.size() + off <= b.size() &&
         std::equal(a.cbegin(), a.cend(), b.cbegin() + off);
}

template <typename CompressedByteReader>
static std::vector<char>
get_block(CompressedByteReader & reader, std::fstream::pos_type const pos,
          std::streamsize const size, std::size_t const block_size)
{
  auto bytes = reader.read_bytes(pos, size & ~SQFS_BLOCK_COMPRESSED_BIT);
  return size & SQFS_BLOCK_COMPRESSED_BIT
             ? bytes
             : reader.comp->decompress(std::move(bytes), block_size);
}

std::optional<fragment_index>
sqsh_writer::dedup_fragment_index(uint32_t inode_number)
{
  auto & cksum = fragmented_checksums[inode_number];
  cksum.update(current_block);
  auto & duplicates = fragmented_duplicates[cksum.sum];
  for (auto dup = duplicates.crbegin(); dup != duplicates.crend(); ++dup)
    {
      auto const & index = fragment_indices[*dup];
      auto const & entry_opt = get_fragment_entry(index.fragment);
      if (entry_opt)
        {
          auto frag = get_block(*this, entry_opt->start_block,
                                entry_opt->size, block_size());
          if (compare_range(current_block, frag, index.offset))
            return fragment_index{index};
        }
      else if (compare_range(current_block, current_fragment, index.offset))
        return fragment_index{index};
    }

  duplicates.push_back(inode_number);
  return {};
}

bool sqsh_writer::dedup_fragment(uint32_t inode_number)
{
  auto const index_opt = dedup_fragment_index(inode_number);
  if (index_opt)
    {
      fragment_indices[inode_number] = *index_opt;
      current_block.clear();
    }
  return bool{index_opt};
}

void sqsh_writer::put_fragment(uint32_t inode_number)
{
  if (dedup_enabled && dedup_fragment(inode_number))
    return;

  if (current_fragment.size() + current_block.size() > block_size())
    flush_fragment();

  auto & index = fragment_indices[inode_number];
  index.fragment = fragment_count;
  index.offset = current_fragment.size();
  current_fragment.insert(current_fragment.end(), current_block.begin(),
                          current_block.end());
  current_block.clear();
}

void sqsh_writer::push_fragment_entry(fragment_entry entry)
{
  {
    std::lock_guard lock(fragments_mutex);
    fragments.push_back(entry);
  }
  fragments_cv.notify_all();
}

std::optional<fragment_entry>
sqsh_writer::get_fragment_entry(uint32_t fragment)
{
  if (fragment == fragment_count)
    return {};
  else
    {
      std::unique_lock lock(fragments_mutex);
      fragments_cv.wait(lock, [&]() { return fragments.size() > fragment; });
      return fragment_entry{fragments[fragment]};
    }
}

void sqsh_writer::write_header()
{
  endian_buffer<SQFS_SUPER_SIZE> header;

  header.l32(SQFS_MAGIC);
  header.l32(next_inode - 1);
  header.l32(0);
  header.l32(1u << super.block_log);
  header.l32(fragments.size());

  header.l16(comp->type);
  header.l16(super.block_log);
  header.l16(super.flags);
  header.l16(ids.size());
  header.l16(SQFS_MAJOR);
  header.l16(SQFS_MINOR);

  header.l64(super.root_inode);
  header.l64(super.bytes_used);
  header.l64(super.id_table_start);
  header.l64(super.xattr_table_start);
  header.l64(super.inode_table_start);
  header.l64(super.directory_table_start);
  header.l64(super.fragment_table_start);
  header.l64(super.lookup_table_start);

  auto end = outfile.tellp();
  end += SQFS_PAD_SIZE - (end % SQFS_PAD_SIZE);

  outfile.seekp(0);
  outfile.write(header.data(), header.size());
  outfile.flush();
  outfile.close();

  std::filesystem::resize_file(outfilepath, end);
}

template <typename Integral> static constexpr auto MASK_LOW(Integral n)
{
  return ~((~0u) << n);
}

template <typename Integral>
static constexpr auto ITD_SHIFT(Integral entry_lb)
{
  return SQFS_META_BLOCK_SIZE_LB - entry_lb;
}

template <typename Integral> static constexpr auto ITD_MASK(Integral entry_lb)
{
  return MASK_LOW(ITD_SHIFT(entry_lb));
}

template <typename Integral>
static constexpr auto ITD_ENTRY_SIZE(Integral entry_lb)
{
  return 1u << entry_lb;
}

template <std::size_t ENTRY_LB, typename EntrySerializer>
static auto write_indexed_table(compressor & comp, std::fstream & outfile,
                                std::size_t const count,
                                EntrySerializer entry)
{
  std::uint64_t const start = outfile.tellp();
  endian_buffer<0> indices;
  metadata_writer mdw(comp);

  for (std::size_t i = 0; i < count; ++i)
    {
      meta_address const maddr = mdw.put(entry(i));
      if ((i & ITD_MASK(ENTRY_LB)) == 0)
        indices.l64(start + maddr.block);
    }

  if (count & ITD_MASK(ENTRY_LB))
    mdw.write_block_no_pad();

  mdw.out(outfile);
  auto const table_start = outfile.tellp();

  outfile.write(indices.data(), indices.size());
  return table_start;
}

static auto write_simple_table(metadata_writer & writer,
                               std::fstream & outfile)
{
  auto const start = outfile.tellp();
  writer.out(outfile);
  return start;
}

void sqsh_writer::write_tables()
{
  super.inode_table_start = write_simple_table(inode_writer, outfile);
  super.directory_table_start = write_simple_table(dentry_writer, outfile);
  super.fragment_table_start = write_indexed_table<4>(
      *comp, outfile, fragments.size(), [this](auto i) {
        auto const [start, size] = fragments[i];
        return make_endian_l<16>(start, size, std::uint32_t{0});
      });
  super.id_table_start =
      write_indexed_table<2>(*comp, outfile, rids.size(), [this](auto i) {
        return make_endian_l<4>(rids[i]);
      });

  super.bytes_used = outfile.tellp();
}

uint16_t sqsh_writer::id_lookup(uint32_t const id)
{
  auto found = ids.find(id);
  if (found == ids.end())
    {
      auto next = ids.size();
      ids[id] = next;
      rids[next] = id;
      return next;
    }
  else
    return found->second;
}

std::vector<char>
sqsh_writer::read_bytes(decltype(outfile)::pos_type const pos,
                        std::streamsize const len)
{
  std::lock_guard lock(outfile_mutex);
  restore_pos rp(outfile);
  std::vector<char> v;

  outfile.seekg(pos);
  v.resize(len);
  outfile.read(v.data(), v.size());

  return v;
}

void sqsh_writer::drop_bytes(decltype(outfile)::off_type count)
{
  std::lock_guard lock(outfile_mutex);
  outfile.seekp(-count, std::ios_base::cur);
}

void sqsh_writer::enqueue_fragment()
{
  if (!writer_failed)
    enqueue(pending_fragment{
        comp->compress_async(std::move(current_fragment), launch_policy())});
  ++fragment_count;
  current_fragment.clear();
}

void sqsh_writer::handle_write(pending_fragment & pending)
{
  compression_result const result = pending.future.get();
  uint64_t const start = write_bytes(result.block);
  push_fragment_entry({start, result.compressed_size()});
}

void sqsh_writer::enqueue_block(uint32_t inode_number)
{
  if (!writer_failed)
    enqueue(pending_block{
        comp->compress_async(std::move(current_block), launch_policy()),
        inode_number});
  current_block.clear();
}

void sqsh_writer::handle_write(pending_block & pending)
{
  compression_result const result = pending.future.get();
  uint64_t const start = write_bytes(result.block);
  auto & report = reports[pending.inode_number];
  if (report.sizes.empty())
    report.start_block = start;
  report.sizes.push_back(result.compressed_size());
}

void sqsh_writer::enqueue_dedup(uint32_t inode_number)
{
  if (dedup_enabled && !writer_failed)
    enqueue(pending_dedup{inode_number});
}

void sqsh_writer::handle_write(pending_dedup pending)
{
  auto const sum = blocked_checksums[pending.inode_number].sum;
  for (auto i : blocked_duplicates[sum])
    if (reports[pending.inode_number].same_content(reports[i], *this))
      {
        drop_bytes(reports[pending.inode_number].range_len());
        reports[pending.inode_number] = reports[i];
        return;
      }
  blocked_duplicates[sum].push_back(pending.inode_number);
}

void sqsh_writer::writer_thread()
{
  for (auto & pending : writer_queue)
    try
      {
        std::visit([this](auto & pending) { handle_write(pending); },
                   pending);
      }
    catch (...)
      {
        writer_failed = true;
      }
}

bool sqsh_writer::finish_data()
{
  flush_fragment();
  writer_queue.finish();
  if (thread.joinable())
    thread.join();
  return writer_failed;
}
