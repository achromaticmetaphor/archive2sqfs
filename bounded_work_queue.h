/*
Copyright (C) 2017  Charles Cagle

This file is part of archive2sqfs.

archive2sqfs is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

archive2sqfs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with archive2sqfs.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LSL_BOUNDED_WORK_QUEUE_H
#define LSL_BOUNDED_WORK_QUEUE_H

#include <condition_variable>
#include <cstddef>
#include <mutex>
#include <optional>
#include <queue>
#include <utility>

template <typename Element> class bounded_work_queue
{
  bool finished;
  std::size_t bound;
  std::queue<Element> queue;
  std::mutex mutex;
  std::condition_variable popped;
  std::condition_variable pushed;

public:
  bounded_work_queue(std::size_t bound) : finished(false), bound(bound) {}

  void push(Element && e)
  {
    {
      std::unique_lock lock(mutex);
      popped.wait(lock, [&]() { return queue.size() < bound; });
      queue.push(std::move(e));
    }
    pushed.notify_all();
  }

  std::optional<Element> pop()
  {
    std::unique_lock lock(mutex);
    pushed.wait(lock, [&]() { return finished || !queue.empty(); });
    if (queue.empty())
      return {};
    auto e = std::move(queue.front());
    queue.pop();
    lock.unlock();
    popped.notify_all();
    return e;
  }

  void finish()
  {
    {
      std::lock_guard guard(mutex);
      finished = true;
    }
    pushed.notify_all();
  }
};

struct bounded_work_queue_iterator_stop
{
};

template <typename Element> struct bounded_work_queue_iterator
{
  bounded_work_queue<Element> & queue;
  std::optional<Element> current;

  bounded_work_queue_iterator & operator++()
  {
    current = queue.pop();
    return *this;
  }

  Element & operator*() { return *current; }

  bool operator!=(bounded_work_queue_iterator_stop) const
  {
    return current.has_value();
  }
};

template <typename Element>
bounded_work_queue_iterator<Element>
begin(bounded_work_queue<Element> & queue)
{
  return {queue, queue.pop()};
}

template <typename Element>
bounded_work_queue_iterator_stop end(bounded_work_queue<Element> const &)
{
  return {};
}

#endif
