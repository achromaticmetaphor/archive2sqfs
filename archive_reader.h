/*
Copyright (C) 2017, 2018  Charles Cagle

This file is part of archive2sqfs.

archive2sqfs is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

archive2sqfs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with archive2sqfs.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LSL_ARCHIVE_READER_H
#define LSL_ARCHIVE_READER_H

#include <cstddef>
#include <stdexcept>
#include <string>

using namespace std::literals;

#include <archive.h>
#include <archive_entry.h>

class archive_reader
{
public:
  archive_reader(char const *);
  archive_reader(std::string const &);
  archive_reader(std::FILE *);
  archive_reader(archive_reader &) = delete;
  archive_reader(archive_reader &&) noexcept;
  ~archive_reader();

  archive_reader & operator=(archive_reader &) = delete;
  archive_reader & operator=(archive_reader &&) noexcept;

  bool next();
  void read(void *, std::size_t);
  template <typename ByteContainer> void read(ByteContainer &, std::size_t);

  auto pathname() const { return archive_entry_pathname(entry); }
  auto filetype() const { return archive_entry_filetype(entry); }
  auto mode() const { return archive_entry_perm(entry); }
  auto uid() const { return archive_entry_uid(entry); }
  auto gid() const { return archive_entry_gid(entry); }
  auto mtime() const { return archive_entry_mtime(entry); }
  auto filesize() const { return archive_entry_size(entry); }
  auto rdev() const { return archive_entry_rdev(entry); }
  auto symlink_target() const { return archive_entry_symlink(entry); }

private:
  struct archive * reader;
  struct archive_entry * entry = nullptr;

  archive_reader();
};

static std::size_t constexpr reader_blocksize = 10240;

inline archive_reader::archive_reader()
{
  reader = archive_read_new();
  if (reader == nullptr)
    throw std::runtime_error("failed to initialize reader object"s);
  if (archive_read_support_filter_all(reader) != ARCHIVE_OK)
    throw std::runtime_error("failed to enable all input filters"s);
  if (archive_read_support_format_all(reader) != ARCHIVE_OK)
    throw std::runtime_error("failed to enable all input formats"s);
}

inline archive_reader::archive_reader(char const * const pathname)
    : archive_reader()
{
  if (archive_read_open_filename(reader, pathname, reader_blocksize) !=
      ARCHIVE_OK)
    throw std::runtime_error("failed to open named archive"s);
}

inline archive_reader::archive_reader(std::string const & pathname)
    : archive_reader(pathname.data())
{
}

inline archive_reader::archive_reader(std::FILE * const handle)
    : archive_reader()
{
  if (archive_read_open_FILE(reader, handle) != ARCHIVE_OK)
    throw std::runtime_error("failed to open archive by handle"s);
}

inline archive_reader::archive_reader(archive_reader && from) noexcept
    : reader(from.reader), entry(from.entry)
{
  from.reader = nullptr;
}

inline archive_reader &
archive_reader::operator=(archive_reader && from) noexcept
{
  reader = from.reader;
  entry = from.entry;
  from.reader = nullptr;
  return *this;
}

inline archive_reader::~archive_reader()
{
  if (reader != nullptr)
    archive_read_free(reader);
}

inline bool archive_reader::next()
{
  auto const result = archive_read_next_header(reader, &entry);
  if (result == ARCHIVE_OK)
    return true;
  if (result == ARCHIVE_EOF)
    return false;
  throw std::runtime_error("error in archive_read_next_header(): "s +
                           archive_error_string(reader));
}

inline void archive_reader::read(void * const buff, std::size_t const len)
{
  if (archive_read_data(reader, buff, len) != len)
    throw std::runtime_error("failed to read data from archive"s);
}

template <typename ByteContainer>
void archive_reader::read(ByteContainer & c, std::size_t const len)
{
  c.resize(len);
  read(c.data(), len);
}

#endif
