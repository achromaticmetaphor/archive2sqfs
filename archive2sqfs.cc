/*
Copyright (C) 2016, 2017, 2018  Charles Cagle

This file is part of archive2sqfs.

archive2sqfs is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

archive2sqfs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with archive2sqfs.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cerrno>
#include <cstring>
#include <iostream>
#include <memory>
#include <optional>
#include <string_view>
#include <thread>
#include <vector>

#include <archive.h>
#include <archive_entry.h>

#include "archive_reader.h"
#include "compressor.h"
#include "dirtree.h"
#include "sqsh_defs.h"
#include "sqsh_writer.h"

using namespace std::literals;

static int usage(std::string_view const & progname)
{
  std::cerr << "usage: "sv << progname
            << " [--single-thread] [--enable-dedup]"sv
            << " [--strip=N] [--compressor=<type>]"sv
            << " [--multiple-archives]"sv
            << " outfile [infile]"sv << std::endl;
  return EINVAL;
}

static constexpr char const * strip_path(size_t const strip,
                                         char const * pathname)
{
  for (size_t i = 0; i < strip; i++)
    {
      char const * const sep = std::strchr(pathname, '/');
      if (sep == nullptr)
        break;
      pathname = sep + 1;
    }

  return pathname;
}

static std::optional<std::string> get_prefix_arg(std::string_view prefix,
                                                 std::string_view arg)
{
  if (arg.substr(0, prefix.size()) == prefix)
    return std::string{arg.substr(prefix.size())};
  return {};
}

template <typename Config>
void process_archive(archive_reader & archive, dirtree_dir & rootdir,
                     Config conf)
{
  std::vector<char> buff;
  while (archive.next())
    {
      auto const pathname = strip_path(conf.strip, archive.pathname());
      auto const filetype = archive.filetype();
      if (filetype == AE_IFDIR)
        rootdir.subdir_for_path(pathname).update_metadata(archive);

      else if (filetype == AE_IFREG)
        {
          auto & reg =
              rootdir.put_file_with_metadata<dirtree_reg>(pathname, archive);

          int64_t i;
          for (i = archive.filesize(); i >= conf.block_size;
               i -= conf.block_size)
            {
              archive.read(buff, conf.block_size);
              reg.append(buff);
            }
          if (i > 0)
            {
              archive.read(buff, i);
              reg.append(buff);
            }

          reg.finalize();
        }

      else if (filetype == AE_IFLNK)
        rootdir.put_file_with_metadata<dirtree_sym>(pathname, archive,
                                                    archive.symlink_target());

      else if (filetype == AE_IFBLK)
        rootdir.put_file_with_metadata<dirtree_dev>(
            pathname, archive, SQFS_INODE_TYPE_BLK, archive.rdev());

      else if (filetype == AE_IFCHR)
        rootdir.put_file_with_metadata<dirtree_dev>(
            pathname, archive, SQFS_INODE_TYPE_CHR, archive.rdev());

      else if (filetype == AE_IFSOCK)
        rootdir.put_file_with_metadata<dirtree_ipc>(pathname, archive,
                                                    SQFS_INODE_TYPE_SOCK);

      else if (filetype == AE_IFIFO)
        rootdir.put_file_with_metadata<dirtree_ipc>(pathname, archive,
                                                    SQFS_INODE_TYPE_PIPE);

      else
        std::cerr << "unsupported filetype: "sv << pathname << std::endl;
    }
}

template <typename Config>
static std::vector<char *> process_arguments(Config & conf, int argc,
                                             char * argv[])
{
  std::vector<char *> extras;
  for (int i = 1; i < argc; ++i)
    if (auto s = get_prefix_arg("--strip="sv, argv[i]); s)
      conf.strip = strtoll(s->c_str(), nullptr, 10);
    else if (auto s = get_prefix_arg("--compressor="sv, argv[i]); s)
      conf.compressor = *s;
    else if ("--single-thread"sv == argv[i])
      conf.single_thread = true;
    else if ("--enable-dedup"sv == argv[i])
      conf.enable_dedup = true;
    else if ("--multiple-archives"sv == argv[i])
      conf.multiarch = true;
    else
      extras.push_back(argv[i]);
  return extras;
}

int main(int argc, char * argv[])
{
  struct
  {
    std::size_t strip = 0;
    int64_t block_size;
    bool multiarch = false;
    bool single_thread = false;
    bool enable_dedup = false;
    std::string compressor = std::string{COMPRESSOR_DEFAULT};
    int block_log = SQFS_BLOCK_LOG_DEFAULT;
  } conf;

  auto const filenames = process_arguments(conf, argc, argv);
  if (filenames.size() < 1 || (filenames.size() > 2 && !conf.multiarch))
    return usage(argv[0]);

  sqsh_writer writer(filenames[0], conf);
  dirtree_dir rootdir(&writer);

  std::vector<archive_reader> archives;
  if (filenames.size() == 1)
    archives.emplace_back(stdin);
  else
    std::copy(filenames.begin() + 1, filenames.end(),
              std::back_inserter(archives));

  conf.block_size = writer.block_size();
  for (archive_reader & archive : archives)
    process_archive(archive, rootdir, conf);

  bool failed = writer.finish_data();
  rootdir.write_tables();
  writer.write_header();

  return failed;
}
