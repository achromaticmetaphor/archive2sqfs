/*
Copyright (C) 2016, 2017, 2018  Charles Cagle

This file is part of archive2sqfs.

archive2sqfs is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

archive2sqfs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with archive2sqfs.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LSL_SQSH_DEFS_H
#define LSL_SQSH_DEFS_H

#include <cstdint>

static std::uint16_t constexpr SQFS_INODE_TYPE_DIR = 8;
static std::uint16_t constexpr SQFS_INODE_TYPE_REG = 9;
static std::uint16_t constexpr SQFS_INODE_TYPE_SYM = 10;
static std::uint16_t constexpr SQFS_INODE_TYPE_BLK = 11;
static std::uint16_t constexpr SQFS_INODE_TYPE_CHR = 12;
static std::uint16_t constexpr SQFS_INODE_TYPE_PIPE = 13;
static std::uint16_t constexpr SQFS_INODE_TYPE_SOCK = 14;

static std::uint32_t constexpr SQFS_MAGIC = 0x73717368u;
static std::uint16_t constexpr SQFS_MAJOR = 4;
static std::uint16_t constexpr SQFS_MINOR = 0;

static std::uintmax_t constexpr SQFS_PAD_SIZE = 0x1000u;
static std::size_t constexpr SQFS_SUPER_SIZE = 96;

static std::size_t constexpr SQFS_META_BLOCK_SIZE_LB = 13;
static std::size_t constexpr SQFS_META_BLOCK_SIZE =
    1 << SQFS_META_BLOCK_SIZE_LB;
static std::uint16_t constexpr SQFS_META_BLOCK_COMPRESSED_BIT = 0x8000u;
static std::uint32_t constexpr SQFS_BLOCK_COMPRESSED_BIT = 0x1000000u;

static std::uint32_t constexpr SQFS_XATTR_NONE = 0xffffffffu;
static std::uint32_t constexpr SQFS_FRAGMENT_NONE = 0xffffffffu;
static std::uint64_t constexpr SQFS_TABLE_NOT_PRESENT = 0xffffffffffffffffu;

static std::uint16_t constexpr SQFS_COMPRESSION_TYPE_ZLIB = 1;
static std::uint16_t constexpr SQFS_COMPRESSION_TYPE_LZMA = 2;
static std::uint16_t constexpr SQFS_COMPRESSION_TYPE_LZO = 3;
static std::uint16_t constexpr SQFS_COMPRESSION_TYPE_XZ = 4;
static std::uint16_t constexpr SQFS_COMPRESSION_TYPE_LZ4 = 5;
static std::uint16_t constexpr SQFS_COMPRESSION_TYPE_ZSTD = 6;

struct meta_address
{
  uint32_t block;
  uint16_t offset;

  meta_address() : block(0), offset(0) {}
  meta_address(uint32_t b, uint16_t o) : block(b), offset(o) {}

  operator uint64_t() const { return (uint64_t(block) << 16) | offset; }
};

#endif
