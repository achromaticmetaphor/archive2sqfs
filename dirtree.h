/*
Copyright (C) 2016, 2017, 2018  Charles Cagle

This file is part of archive2sqfs.

archive2sqfs is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

archive2sqfs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with archive2sqfs.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LSL_DIRTREE_H
#define LSL_DIRTREE_H

#include <cstdint>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <string_view>
#include <vector>

#include "sqsh_defs.h"
#include "sqsh_writer.h"

struct dirtree
{
  uint16_t inode_type;
  uint16_t mode = 0644;
  uint32_t uid = 0;
  uint32_t gid = 0;
  uint32_t mtime = 0;
  uint32_t inode_number;
  meta_address inode_address;
  uint32_t nlink = 1;
  uint32_t xattr = SQFS_XATTR_NONE;
  sqsh_writer * wr;

  dirtree(sqsh_writer * wr, uint16_t inode_type)
      : inode_type(inode_type), inode_number(wr->next_inode_number()), wr(wr)
  {
  }

  template <typename MetadataSource>
  void update_metadata(MetadataSource const & ms)
  {
    mode = ms.mode();
    uid = ms.uid();
    gid = ms.gid();
    mtime = ms.mtime();
  }

  virtual void write_inode(uint32_t) = 0;
  void write_tables();

  virtual ~dirtree() = default;
  dirtree(dirtree &) = delete;
  dirtree(dirtree &&) = delete;
  dirtree & operator=(dirtree &) = delete;
  dirtree & operator=(dirtree &&) = delete;
};

struct dirtree_ipc : public dirtree
{
  dirtree_ipc(sqsh_writer * wr, uint16_t inode_type) : dirtree(wr, inode_type)
  {
  }

  void write_inode(uint32_t) override;
};

struct dirtree_reg : public dirtree
{
  uint64_t file_size = 0;
  uint64_t sparse = 0;

  std::size_t block_count = 0;

  dirtree_reg(sqsh_writer * wr) : dirtree(wr, SQFS_INODE_TYPE_REG) {}

  void append(char const *, std::size_t);
  void flush();
  void finalize();
  void write_inode(uint32_t) override;

  template <typename ByteContainer> void append(ByteContainer & con)
  {
    append(con.data(), con.size());
  }
};

struct dirtree_sym : public dirtree
{
  std::string target;

  dirtree_sym(sqsh_writer * wr, std::string_view target)
      : dirtree(wr, SQFS_INODE_TYPE_SYM), target(target)
  {
  }

  void write_inode(uint32_t) override;
};

struct dirtree_dev : public dirtree
{
  uint32_t rdev;

  dirtree_dev(sqsh_writer * wr, uint16_t type, uint32_t rdev)
      : dirtree(wr, type), rdev(rdev)
  {
  }

  void write_inode(uint32_t) override;
};

struct dirtree_dir : public dirtree
{
  std::map<std::string, std::unique_ptr<dirtree>, std::less<>> entries;
  uint32_t filesize;
  uint32_t dtable_start_block;
  uint16_t dtable_start_offset;

  dirtree_dir(sqsh_writer * wr) : dirtree(wr, SQFS_INODE_TYPE_DIR) {}

  dirtree & put_child(std::string_view name,
                      std::unique_ptr<dirtree> && child)
  {
    return *(entries[std::string{name}] = std::move(child));
  }

  dirtree_dir & get_subdir(std::string_view);
  void write_inode(uint32_t) override;
  dirtree_dir & subdir_for_path(std::string_view);
  dirtree & put_file(std::string_view, std::unique_ptr<dirtree> &&);

  template <typename Dirtree, typename... ContructorArguments>
  Dirtree & put_file(std::string const & name, ContructorArguments... a)
  {
    return static_cast<Dirtree &>(
        put_file(name, std::make_unique<Dirtree>(wr, a...)));
  }

  template <typename Dirtree, typename MetadataSource,
            typename... ContructorArguments>
  Dirtree & put_file_with_metadata(std::string const & name,
                                   MetadataSource const & ms,
                                   ContructorArguments... a)
  {
    Dirtree & file = put_file<Dirtree>(name, a...);
    file.update_metadata(ms);
    return file;
  }
};

#endif
